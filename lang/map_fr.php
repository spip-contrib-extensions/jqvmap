<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_map' => 'Ajouter cette carte',

	// C
	'carte_importee' => 'La carte a été importée avec succès',
	'carte_maj' => 'La carte a été mise à jour avec succès',
	'champ_background_color_label' => 'Couleur du fond',
	'champ_border_color_label' => 'Couleur des bordures',
	'champ_border_opacity_explication' => 'La valeur à renseigner doit être comprise entre 0 et 1.',
	'champ_border_opacity_label' => 'Opacité des bordures',
	'champ_border_width_label' => 'Épaisseur de la bordure',
	'champ_code_map_label' => 'Code unique',
	'champ_color_label' => 'Couleur des régions',
	'champ_data_name_label' => 'Nom de la variable de data visualisation',
	'champ_descriptif_label' => 'Descriptif',
	'champ_enable_zoom_explication' => 'Activer le zoom sur la carte.',
	'champ_enable_zoom_label' => 'Zoom',
	'champ_height_label' => 'Hauteur',
	'champ_hover_color_explication' => 'Couleur de la région lorsque le pointeur de la souris est au-dessus.',
	'champ_hover_color_label' => 'Couleur de survol',
	'champ_hover_opacity_explication' => 'La valeur à renseigner doit être comprise entre 0 et 1.',
	'champ_hover_opacity_label' => 'Opacité au survol',
	'champ_normalize_function_explication' => 'Cette fonction peut être utilisée pour améliorer les résultats de visualisations des données de nature non-linéaire. ',
	'champ_normalize_function_label' => 'Fonction normalisée',
	'champ_scale_colors_explication' => 'Cette option définit les couleurs, avec laquelle les régions seront peints lorsque vous définissez les valeurs d\'option. Elle peut contenir plusieurs valeurs hexadécimal, séparées par une virgule.',
	'champ_scale_colors_label' => 'Couleurs de l\'échelle',
	'champ_selected_color_explication' => 'Valeur héxadécimale. Par défaut #c9dfaf',
	'champ_selected_color_label' => 'Couleur des éléments sélectionnés',
	'champ_selected_region_explication' => 'Ceci est la région que vous cherchez à avoir présélectionné.',
	'champ_selected_region_label' => 'Zone présélectionnée de la carte',
	'champ_show_tooltip_label' => 'Info-bulle',
	'champ_titre_label' => 'Titre',
	'champ_width_label' => 'Largeur',
	'choix_oui' => 'Oui',
	'choix_non' => 'Non',

	// E
	'erreur_code_map_deja' => 'Ce code unique est déjà utilisé par une carte.',
	'erreur_code_map_forme' => 'Le code unique ne doit contenir que des lettres non accentuées, des chiffres ou le caractère souligné.',
	'erreur_data_name_forme' => 'Le nom de la data ne doit contenir que des lettres non accentuées, des chiffres ou le caractère souligné.',
	'erreur_upload_xml_type' => 'Veuillez charger un fichier XML avec son extension <em>.xml</em>.',

	// I
	'icone_creer_map' => 'Créer une carte',
	'icone_exporter_map' => 'Exporter cette carte',
	'icone_modifier_map' => 'Modifier cette carte',
	'info_1_map' => 'Une carte',
	'info_aucun_map' => 'Aucune carte',
	'info_maps_auteur' => 'Les cartes de cet auteur',
	'info_nb_maps' => '@nb@ cartes',

	// L
	'liste_maps_xml_explication' => 'Ces fichiers sont à disposition dans le(s) répertoire(s) "jqvmap_xml"',
	'liste_maps_xml_label' => 'À partir des templates',
	'import_maps_xml_explication' => 'Ces fichiers sont à disposition dans le(s) répertoire(s) "jqvmap_xml"',
	'import_maps_xml_label' => 'Charger un fichier',

	// N
	'normalize_function_linear' => 'linear',
	'normalize_function_polynomial' => 'polynomial',

	// R
	'retirer_lien_map' => 'Retirer cette carte',
	'retirer_tous_liens_maps' => 'Retirer toutes les cartes',

	// T
	'texte_ajouter_map' => 'Ajouter une carte',
	'texte_changer_statut_map' => 'Cette carte est :',
	'texte_creer_associer_map' => 'Créer et associer une carte',
	'texte_definir_comme_traduction_map' => 'Cette carte est une traduction de la carte numéro :',
	'titre_importer_map' => 'Importer une carte',
	'titre_langue_map' => 'Langue de cette carte',
	'titre_logo_map' => 'Logo de cette carte',
	'titre_map' => 'Carte',
	'titre_maps' => 'Cartes',
	'titre_maps_rubrique' => 'Cartes de la rubrique',
);
