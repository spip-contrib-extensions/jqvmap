<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_vector' => 'Ajouter ce vecteur',

	// C
	'champ_code_vector_label' => 'Abbréviation de la zone',
	'champ_color_label' => 'Couleur de la zone',
	'champ_data_label' => 'Data visualisation',
	'champ_descriptif_label' => 'Descriptif',
	'champ_id_map_label' => 'Carte',
	'champ_path_label' => 'Coordonnées',
	'champ_target_blank' => 'Oui',
	'champ_target_label' => 'Ouvrir le lien dans une nouvelle fenêtre&nbsp;?',
	'champ_target_self' => 'Non',
	'champ_titre_label' => 'Titre',
	'champ_url_site_label' => 'Lien hypertexte',
	'confirmer_supprimer_vector' => 'Confirmez-vous la suppression de ce vecteur ?',

	// I
	'icone_creer_vector' => 'Créer un vecteur',
	'icone_modifier_vector' => 'Modifier ce vecteur',
	'info_1_vector' => 'Un vecteur',
	'info_aucun_vector' => 'Aucun vecteur',
	'info_nb_vectors' => '@nb@ vecteurs',
	'info_vectors_auteur' => 'Les vecteurs de cet auteur',

	// R
	'retirer_lien_vector' => 'Retirer ce vecteur',
	'retirer_tous_liens_vectors' => 'Retirer tous les vecteurs',

	// S
	'supprimer_vector' => 'Supprimer ce vecteur',

	// T
	'texte_ajouter_vector' => 'Ajouter un vecteur',
	'texte_changer_statut_vector' => 'Ce vecteur est :',
	'texte_creer_associer_vector' => 'Créer et associer un vecteur',
	'texte_definir_comme_traduction_vector' => 'Ce vecteur est une traduction du vecteur numéro :',
	'titre_langue_vector' => 'Langue de ce vecteur',
	'titre_logo_vector' => 'Logo de ce vecteur',
	'titre_vector' => 'Vecteur',
	'titre_vectors' => 'Vecteurs',
	'titre_vectors_rubrique' => 'Vecteurs de la rubrique',
);
